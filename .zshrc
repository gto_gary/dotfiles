#####
# Power Level 10k conig
#####

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#####
# Exports and path updates
#####

# If local bin dirs exist add them to $PATH
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#####
#VAR EXPORTS
#####

export ZSH="$HOME/.oh-my-zsh"
export OPENAI_API_KEY="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"
export NIXPKGS_ALLOW_UNFREE=1
export NIXPKGS_ALLOW_INSECURE=1
export EDITOR=nvim

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#####
# ohmyzsh/zsh config
#####

#Plugins
plugins=(
    git 
    zsh-autosuggestions 
    zsh-syntax-highlighting
    sudo
    web-search
    copypath
    copyfile
    copybuffer
    virtualenv
)

source $ZSH/oh-my-zsh.sh

#####
#WSL Specific
#####

#alias gdir='cd /mnt/c/Users/golson'

## WSL clear screen
#/usr/bin/clear

#####
#alias
#####
alias chatgpt="cd ~/workspace/shellgpt/;source shellgpt/bin/activate"
alias vi="nvim"
alias config='/usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary'
alias configaa='/usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary status -s | awk "{ print \$2 }"|xargs /usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary add'
alias nixsc='ln -s /home/$USER/.nix-profile/share/applications/* /home/$USER/.local/share/applications'
alias hms='home-manager switch --impure'
alias hm-update="sudo -i nix-channel --update && home-manager switch --impure"
alias ssh='TERM=xterm-256color \ssh'
alias lgit='lazygit'
