" install the following:
" git clone https://github.com/vim-airline/vim-airline ~/.vim/pack/dist/start/vim-airline
" git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/pack/dist/start/vim-airline-themes
" git clone --depth 1 https://github.com/sheerun/vim-polyglot ~/.vim/pack/plugins/start/vim-polyglot
" git clone https://github.com/preservim/nerdtree.git ~/.vim/pack/vendor/start/nerdtree
" vim -u NONE -c "helptags ~/.vim/pack/vendor/start/nerdtree/doc" -c q

filetype plugin on
syntax on
" set laststatus=2 " Always display the statusline in all windows
" set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set t_Co=256
set tabstop=2
set shiftwidth=2
set expandtab
set number
set showmatch
set ignorecase
set hlsearch
set incsearch
set autoindent
set softtabstop=2
set splitbelow
set termwinsize=12x0
set mouse=a
set ttymouse=sgr
let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers
let g:airline_theme='jellybeans'
let g:airline_powerline_fonts = 1
python3 <<EOF
import sys; sys.path.append("/usr/lib/python3.11/site-packages/")
EOF
