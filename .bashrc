# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH
export EDITOR=nvim
export NIXPKGS_ALLOW_UNFREE=1
export NIXPKGS_ALLOW_INSECURE=1

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

#tilix fix
#if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#        source /etc/profile.d/vte.sh
#fi

#if [ -f `which powerline-daemon` ]; then
#  powerline-daemon -q
#  POWERLINE_BASH_CONTINUATION=1
#  POWERLINE_BASH_SELECT=1
#  . /usr/share/powerline/bindings/bash/powerline.sh
#fi

#####
#Alias
#####

alias ll='ls -alF'
alias vi='nvim'
alias config='/usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary'
alias configaa='/usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary status -s | awk "{ print \$2 }"|xargs /usr/bin/git --git-dir=/home/gary/.dotfiles/ --work-tree=/home/gary add'
alias nixsc='ln -s /home/$USER/.nix-profile/share/applications/* /home/$USER/.local/share/applications'
alias hms='home-manager switch --impure'
alias hm-update="sudo -i nix-channel --update && home-manager switch --impure"
