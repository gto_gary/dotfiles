# Dotfile using bare repo

- Based on article here: https://www.atlassian.com/git/tutorials/dotfiles

## Setup of the orign machine

``` /bin/bash
# Create the bar local repo
git init --bare $HOME/.dotfiles

# Create the config alias in the existing shell
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Do not show untracked files
config config --local status.showUntrackedFiles no

# add alias to bashrc and zshrc (if used)
echo "alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
echo "alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.zshrc
```

**Use the config command for all git activities for dotfiles.**

## Setup on a new machine

``` /bin/bash

# cone the bare repo
git clone --bare https://gitlab.com/gto_gary/dotfiles.git $HOME/.dotfiles

# add alias to existing shell
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Move any existing files to .config-backup
mkdir -p mkdir -p .config-backup/.config/home-manager && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
rm -f ~/README.md

# checkout 
config checkout

# disable shoe untracked files
config config --local status.showUntrackedFiles no

```

**Use the config command for all git activities for dotfiles.**