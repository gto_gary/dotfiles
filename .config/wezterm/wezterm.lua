
--LOCALS
local wezterm = require 'wezterm'
local mux = wezterm.mux
local scheme = wezterm.get_builtin_color_schemes()['PencilDark']
local act = wezterm.action
scheme.background = '#151515'
scheme.foreground = '#f1f1f1'

return {
  color_schemes = {
    ['garydark1'] = scheme,
  },
  color_scheme = 'garydark1',
  --color_scheme = 'PencilDark',
  font = wezterm.font 'SFMono Nerd Font',
  font_size = 13,
  hide_tab_bar_if_only_one_tab = true,
}  
