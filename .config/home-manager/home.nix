{pkgs, ...}: {
  home.username = "gary";
  home.homeDirectory = "/home/gary";
  home.stateVersion = "24.11"; # To figure this out you can comment out the line and see what version it expected.
  home.packages = [
    pkgs.google-chrome

  ];
  programs.home-manager.enable = true;
}
